<?php
ob_start();
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Weblang extends CI_Controller {

	public function switchlang($la = "") {

        $lan = ($la != "") ? $la : "en";

        $this->session->set_userdata('site_lang', $lan);
        if($this->session->userdata('site_lang') == 'th'){
					// $this->load->language('mes','thai');
        $this->lang->load('mes','thai');
			}elseif($this->session->userdata('site_lang') == 'en'){
					//  $this->load->language('mes','english');

        $this->lang->load('mes','english');
        }

       redirect($_SERVER['HTTP_REFERER']);
  }


}

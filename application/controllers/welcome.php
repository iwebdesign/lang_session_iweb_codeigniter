<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->library('check_lang');
		$this->load->view('test_lang');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php

class Check_lang {

  public function __construct()
  {

    $ci =& get_instance();
    $ci->load->library('session');
    $ci->load->helper('url');
    $ci->load->helper('language');
    if($ci->session->userdata('site_lang')==""||$ci->session->userdata('site_lang')==false){
      $ci->session->set_userdata('site_lang','en');
      $ci->lang->load('mes','english');
    }else{
      if($ci->session->userdata('site_lang') == 'th'){
        $ci->lang->load('mes','thai');
      }elseif($ci->session->userdata('site_lang') == 'en'){
        $ci->lang->load('mes','english');
      }
    }

    //redirect($_SERVER['HTTP_REFERER']);



  }



}
